<!DOCTYPE html>
<html lang="en">
<head>
    <title>Math</title>
</head>
<body>
    <?php
        echo "Menampilkan nilai phi : ". pi(). "<br>";
        echo "Mencari nilai max dari 3, 6, 7 , 9, 1 = ". max(3, 6, 7, 9, 1). "<br>";
        echo "Mencari nilai min dari 3, 6, 7 , 9, 1 = ". min(3, 6, 7, 9, 1). "<br>";
        echo "Menampilkan nilai absolute dari -9 = ". abs(-9). "<br>";
        echo "Menampilkan akar kuadrat dari 81 = ". sqrt(81). "<br>";
        echo "Menampilkan bilang bulat terdekat untuk 2.432 = ". round(2.432). "<br>";
        echo "Menampilkan bilangan random tanpa range = ". rand(). "<br>";
        echo "Menampilkan bilangan random dari 566 sampai 666 = ". rand(566, 666). "<br>";
    ?>
</body>
</html>