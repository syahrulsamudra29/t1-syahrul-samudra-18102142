<!DOCTYPE html>
<html lang="en">
<head>
    <title>Function</title>
</head>
<body>
    <?php
        function luassegitiga(int $a, int $t){
            $l = 1/2 * $a * $t;
            return "Luas segi tiga dengan alas $a dan tinggi $t adalah = ". $l;
        }

        echo luassegitiga(10, 5);
    ?>
</body>
</html>