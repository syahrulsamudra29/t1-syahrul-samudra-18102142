<!DOCTYPE html>
<html lang="en">
<head>
    <title>Loops</title>
</head>
<body>
    <?php
        $i = 0;

        echo "Loop dibawah dibuat menggunakan do-while <br>";
        do {
            echo "ini perulangan ke-". $i. "<br>";
            $i++;
        } while ($i <= 10);

        echo "<br> Loop dibawah  dibuat menggunakan for <br>";
        for ($a=0; $a <= 10; $a++) {
            echo "ini perulangan ke-". $a. "<br>";
        }
    ?>
</body>
</html>