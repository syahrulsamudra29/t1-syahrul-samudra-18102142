<!DOCTYPE html>
<html lang="en">
<head>
    <title>Array Foreach</title>
</head>
<body>
    <?php
        $makanan = array('Sate','Nasi Goreng', 'Seblack', 'Magelangan', 'Indomi Seleraku');

        echo "Dengan Angka :<br>";
        foreach ($makanan as $i => $v) {
            echo "$i = $v <br>";
        }

        echo "<br> Tanpa Angka :<br>";
        foreach ($makanan as $i => $v) {
            echo "$v <br>";
        }
    ?>
</body>
</html>