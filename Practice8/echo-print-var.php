<!DOCTYPE html>
<html lang="en">
<head>
    <title>Echo, Print and Variable</title>
</head>
<body>
    <?php
        $nama = "Syahrul";
        $makanan = "Nasi Goreng";
        $moto = "Wakwaw";

        echo "Variabel dipanggil menggunakan echo <br>";
        echo "Maknan favorit ". $nama. " adalah ". $makanan;

        echo "<br><br>";
        echo "Variabel dipanggil menggunakan print <br>";
        print "Moto hidup dari ". $nama. " adalah ". $moto;

        echo "<br><br>";
        echo "harusnya dibawah ada tulisan tapi di comment xixixi<br>";
        
        /* echo "Maknan favorit ". $nama. " adalah ". $makanan;
        print "Moto hidup dari ". $nama. " adalah ". $moto; */
    ?>
</body>
</html>