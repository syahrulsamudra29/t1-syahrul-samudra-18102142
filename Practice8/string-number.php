<!DOCTYPE html>
<html lang="en">
<head>
    <title>String and Number</title>
</head>
<body>
    <?php
        $tulisan = "Tulisan ini yang akan di ujikan";
        echo "Akan mengolah tulisan ini <br>". "'". $tulisan. "' <br>". "yang di simpan dalam variabel. <br><br>";

        echo "Menghitung panjang string = ". strlen($tulisan). "<br>";
        echo "Menghitung jumlah kata = ". str_word_count($tulisan). "<br>";
        echo "Membalik kata = ". strrev($tulisan). "<br>";
        echo "Men-replace string = ". str_replace("ujikan", "dicoba", $tulisan). "<br><br>";

        $intejer = 45;
        $desimal  = 7.212;
        echo "Mencari tahu tipe data : <br>";
        echo " : isi dari variabel intejer". var_dump($intejer). "<br>";
        echo " : isi dari variabel desiaml". var_dump($desimal). " <br>";

        echo "<br> Uji Numerik : <br>";
        echo " : untuk variabel intejer". var_dump(is_numeric($intejer)). "<br>";
        echo " : untuk variabel desimal". var_dump(is_numeric($desimal)). "<br>";
        echo " : untuk variabel tulisan". var_dump(is_numeric($tulisan)). "<br>";
    ?>
</body>
</html>