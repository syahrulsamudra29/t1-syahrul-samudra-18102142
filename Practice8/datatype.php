<!DOCTYPE html>
<html lang="en">
<head>
    <title>Data Type</title>
</head>
<body>
    <?php
        $kata = "Syahrul Samudra";
        $angka = 56;
        $desimal = 5.99;

        echo var_dump($kata)."<br>";
        echo var_dump($angka)."<br>";
        echo var_dump($desimal)."<br><br>";
        class menu {
            public $namapaket;
            public $makanan;
            public $minuman;
            public function __construct($namapaket, $makanan, $minuman) {
                $this->namapaket = $namapaket;
                $this->makanan = $makanan;
                $this->minuman = $minuman;
            }

            public function message() {
                return "Ini adalah Paket " . $this->namapaket . " isinya " . $this->makanan . " dan minuman ". $this -> minuman;
            }
        }

        $mymenu = new menu("Anak Kos", "Nasi Goreng", "Teh");
        echo $mymenu -> message();

        echo "<br>";

        $mymenu = new menu("Hedon", "Iga Bakar", "Es teh");
        echo $mymenu -> message();
    ?>
</body>
</html>