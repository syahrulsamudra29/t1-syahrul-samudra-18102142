<!DOCTYPE html>
<html lang="en">
<head>
    <title>Constans</title>
</head>
<body>
    <?php
        echo "contoh menampilkan dengan constans :<br>";
        define('makanan', 'Nasi Goreng');
        echo makanan;
        
        echo "<br><br>";
        echo "Constant Array memanggil indeks 2 :<br>";
        define("burjo", [
            "Omlet",
            "Intel Goreng",
            "Magelangan",
            "wakwaw"
        ]);
        echo burjo[2];
        
    ?>
</body>
</html>