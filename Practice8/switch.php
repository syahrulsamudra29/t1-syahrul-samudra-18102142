<!DOCTYPE html>
<html lang="en">
<head>
    <title>Switch</title>
</head>
<body>
    <?php
        $pilihan = 5;

        switch ($pilihan) {
            case '1':
                echo "Nilai di variabel pilihan adalah 1";
                break;
            case '2':
                echo "Nilai di variabel pilihan adalah 2";
                break;
            case '3':
                echo "Nilai di variabel pilihan adalah 3";
                break;
            default:
                echo "nilai di variabel pilihan melebihi case atau tidak ada";
                break;
        }
    ?>
</body>
</html>