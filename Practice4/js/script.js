window.alert('Halaman Ini menggunakan Embedded Script');

document.getElementById("hitung").onclick = function () { hitungluas() };

function hitungluas() {
    var a = document.getElementById("tinggi").value;
    var b = document.getElementById("alas").value;
    var hasil = 0.5 * b * a;
    document.getElementById("hasil").innerHTML = hasil;
}

function konversi() {
    var nilaiAngka = document.getElementsByClassName("nilai_angka");
    if (nilaiAngka[0].value >= 90) grade = "A"
    else if (nilaiAngka[0].value >= 80) grade = "B"
    else if (nilaiAngka[0].value >= 70) grade = "B+"
    else if (nilaiAngka[0].value >= 60) grade = "C+"
    else if (nilaiAngka[0].value >= 50) grade = "C"
    else if (nilaiAngka[0].value >= 40) grade = "D+"
    else if (nilaiAngka[0].value >= 30) grade = "E"
    else grade = "F";

    document.getElementById("hasil2").innerHTML = grade;
}

function hitungTotal() {
    var jumlah = document.getElementById("jumlah").value;
    var harga = document.getElementById("harga").value;

    if (jumlah == "" || harga == "") {
        document.getElementById('total').value = 0;
    } else {
        document.getElementById('total').value = jumlah * harga;
    }
}

function cetak() {
    window.print();
}